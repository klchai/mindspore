![MindSpore Logo](docs/MindSpore-logo.png "MindSpore logo")
============================================================

- [什么是MindSpore?](#what-is-mindspore)
  - [自动微分](#automatic-differentiation)
  - [自动并行](#automatic-parallel)
- [安装](#installation)
  - [版本](#binaries)
  - [从源代码编译](#from-source)
  - [使用 Docker 镜像](#docker-image)
- [快速入门](#quickstart)
- [文档](#docs)
- [社区](#community)
  - [管理](#governance)
  - [交流](#communication)
- [贡献](#contributing)
- [发行说明](#release-notes)
- [协议](#license)

## 什么是 MindSpore

MindSpore是一个新的开源深度学习训练/推理框架，可用于移动、边缘和云计算等场景。MindSpore旨在为数据科学家和算法工程师提供友好的设计和高效执行的开发体验，原生支持Ascend AI处理器，同时做了软硬件协同优化。同时，MindSpore作为一个全球性的人工智能开源社区，旨在进一步推进人工智能软、硬件应用生态系统的发展和丰富。

<img src="docs/MindSpore-architecture.png" alt="MindSpore Architecture" width="600"/>

更多详情请查看我们的 [架构指南](https://www.mindspore.cn/docs/en/0.1.0-alpha/architecture.html)。

### 自动微分

目前主流的深度学习框架中，有三种自动区分技术。

- **基于静态计算图进行转换**: 在编译时将网络转化为静态的数据流图，然后将链式规则应用到数据流图上，以实现自动微分。

- **基于动态计算图进行转换**: 以操过载的方式记录网络在前向执行过程中的操作轨迹，然后将链式规则应用到动态生成的数据流图上，以实现自动微分。

- **基于源代码进行转换**: 该技术从函数式编程框架发展而来，以just-in-time编译（JIT）的形式对中间表达式（程序在编译过程中的表达形式）进行自动微分变换，支持复杂的控制流场景、高阶函数和闭包等。

TensorFlow早期采用的是静态计算图，而PyTorch采用的是动态计算图。静态计算图可以利用静态编译技术来优化网络性能，但是，构建网络或者调试网络是非常复杂的。使用动态计算图非常方便，但在性能上很难达到极致优化。

但MindSpore采取了另一种方式，基于源码转换的自动微分。一方面，它支持自动微分的自动控制流，所以构建像PyTorch这样的模型相当方便。另一方面，MindSpore可以对神经网络进行静态编译优化，实现了神经网络的静态编译优化，从而实现了很好的性能。

<img src="docs/Automatic-differentiation.png" alt="Automatic Differentiation" width="600"/>

MindSpore自动微分的实现可以理解为程序本身的符号微分。由于MindSpore IR是一个函数的中间表达式，所以它与基本代数中的复合函数有直接对应关系。由任意基本函数组成的复合函数的派生公式可以推导出由任意基本函数组成的复合函数的派生公式。MindSpore IR中的每一个基元运算都可以对应于基本代数中的基本函数，可以构建更复杂的流控。

### 自动并行

MindSpore自动并行的目标是建立一种结合数据并行、模型并行、混合并行的训练方法。它可以自动选择最小成本的模型拆分策略，实现自动分布式并行训练。

<img src="docs/Automatic-parallel.png" alt="Automatic Parallel" width="600"/>

目前，MindSpore采用的是精细化的并行策略，即把图中的每一个运算符拆分到一个集群中完成并行操作。这期间的拆分策略可能非常复杂，但作为提倡Pythonic的开发者，你不需要关心底层实现，只要顶层API计算效率高就可以了。

## 安装

### 版本

MindSpore 提供跨多个后端的构建选项：

| 硬件平台 | 操作系统 | 可用性 |
| :---------------- | :--------------- | :----- |
| Ascend910 | Ubuntu-x86 | ✔️ |
| | EulerOS-x86 | ✔️ |
| | EulerOS-aarch64 | ✔️ |
| GPU CUDA 9.2 | Ubuntu-x86 | ✔️ |
| GPU CUDA 10.1 | Ubuntu-x86 | ✔️ |
| CPU | Ubuntu-x86 | ✔️ |
| | Windows-x86 | ✔️ |

使用 `pip`安装，以`Ubuntu-x86`平台`CPU`构建版本为例。

1. 从 [MindSpore 下载页面](https://www.mindspore.cn/versions/en)，下载whl包并安装。
```
pip install https://ms-release.obs.cn-north-4.myhuaweicloud.com/0.1.0-alpha/MindSpore/cpu/ubuntu-x86/mindspore-0.1.0-cp37-cp37m-linux_x86_64.whl
```
2. 运行下列指令验证安装。
```
python -c 'import mindspore'
```
### 从源代码编译

[安装 MindSpore](https://www.mindspore.cn/install/en).

### 使用 Docker 镜像

MindSpore docker 镜像托管在 [Docker Hub](https://hub.docker.com/r/mindspore),

目前支持的容器化构建选项如下：

| 硬件平台 | Docker 镜像仓库 | 标签 | 描述 |
| :---------------- | :---------------------- | :-- | :---------- |
| CPU | `mindspore/mindspore-cpu` | `0.1.0-alpha` | 预装了MindSpore `0.1.0-alpha` CPU版本的生产环境。 |
| | | `devel` | 从源代码构建MindSpore开发环境（带 `CPU `后端），详情请参考https://www.mindspore.cn/install/en。 |
| | | `runtime` | 提供运行环境来安装MindSpore二进制包与`CPU`后端。 |
| GPU | `mindspore/mindspore-gpu` | `0.1.0-alpha` | 预装了MindSpore `0.1.0-alpha` GPU版本的生产环境。 |
| | | `devel` | 从源代码构建MindSpore开发环境（带 `GPU CUDA10.1` 后端），详情请参考https://www.mindspore.cn/install/en。 |
| | | `runtime` | 提供运行环境来安装MindSpore二进制包与`GPU`后端。 |
| Ascend | <center>&mdash;</center> | <center>&mdash;</center> | 即将推出。 |


* CPU
  对于 `CPU` 后端，你可以用下列命令直接拉取并运行镜像：
```
  docker pull mindspore/mindspore-cpu:0.1.0-alpha
  docker run -it mindspore/mindspore-cpu:0.1.0-alpha python -c 'import mindspore'
```

* GPU

  对于 `GPU` 后端, 请确保 `nvidia-container-toolkit` 已经按照，这里是给 `Ubuntu` 用户的一些安装指南：
```
  DISTRIBUTION=$(. /etc/os-release; echo $ID$VERSION_ID)
  curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | apt-key add -
  curl -s -L https://nvidia.github.io/nvidia-docker/$DISTRIBUTION/nvidia-docker.list | tee /etc/apt/sources.list.d/nvidia-docker.list
  sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit nvidia-docker2
  sudo systemctl restart docker
```
 然后你可以用下列命令直接拉取并运行镜像：

```
  docker pull mindspore/mindspore-gpu:0.1.0-alpha
  docker run -it --runtime=nvidia --privileged=true mindspore/mindspore-gpu:0.1.0-alpha /bin/bash
```

 检测 docker 镜像是否能运行，请执行下列 Python 代码，并检查输出：
```python
  import numpy as np
  from mindspore import Tensor
  from mindspore.ops import functional as F
  import mindspore.context as context
  
  context.set_context(device_target="GPU")
  x = Tensor(np.ones([1,3,3,4]).astype(np.float32))
  y = Tensor(np.ones([1,3,3,4]).astype(np.float32))
  print(F.tensor_add(x, y))

```
```
  [[[ 2. 2. 2. 2.],
  [ 2. 2. 2. 2.],
  [ 2. 2. 2. 2.]],
  [[ 2. 2. 2. 2.],
  [ 2. 2. 2. 2.],
  [ 2. 2. 2. 2.]],
  [[ 2. 2. 2. 2.],
  [ 2. 2. 2. 2.],
  [ 2. 2. 2. 2.]]]
```

如果你想了解更多关于 MindSpore docker 镜像的构建过程，请查看`docker`文件夹了解详情。

## 快速入门

查询 [快速入门](https://www.mindspore.cn/tutorial/en/0.1.0-alpha/quick_start/quick_start.html) 来实现图像分类。


## 文档

更多关于安装指南、教程和API的详细内容，请参见以下内容：[User Documentation](https://gitee.com/mindspore/docs).

## 社区

### 管理

查看 MindSpore 开放管理如何[运作](https://gitee.com/mindspore/community/blob/master/governance.md)。

### 交流

- [MindSpore Slack](https://join.slack.com/t/mindspore/shared_invite/enQtOTcwMTIxMDI3NjM0LTNkMWM2MzI5NjIyZWU5ZWQ5M2EwMTQ5MWNiYzMxOGM4OWFhZjI4M2E5OGI2YTg3ODU1ODE2Njg1MThiNWI3YmQ) - 开发者沟通平台。
- IRC channel 上的 `#mindspore`(仅用于记录会议记录)
- 视频会议: https://meet.jit.si
- 邮件列表: https://mailweb.mindspore.cn/postorius/lists

## 贡献

欢迎做出贡献。详情请参见 [贡献](CONTRIBUTING.md)。

## 发行说明

发行说明请参见 [发行](RELEASE.md)。

## 协议

[Apache License 2.0](LICENSE)